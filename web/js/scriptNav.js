/**
 * Created by nadjemmejdan on 21/04/2017.
 */
$(document).ready(function(){



    var stylesDisabled = {
        borderBottom : "solid 1px rgba(0, 0, 0, 0.25)",
        backgroundColor: "rgba(0, 0, 0, 0.25)",
        color: "black"
    };
    var stylesEnable = {
        borderBottom : "none",
        backgroundColor: "white",
        color: "#f56b2a"

    };


    $('#proBtn').css(stylesDisabled);
    $('#partBtn').css(stylesDisabled);
    $('#toutesBtn').css(stylesEnable);

    $('#toutesBtn').click(function () {


        $('#proBtn').css(stylesDisabled);
        $('#partBtn').css(stylesDisabled);
        $('#toutesBtn').css(stylesEnable);


        $('#part').css("display","none");
        $('#pro').css("display","none");
        $('#all').css("display","block");

    });

    $('#proBtn').click(function () {

        $('#proBtn').css(stylesEnable);
        $('#partBtn').css(stylesDisabled);
        $('#toutesBtn').css(stylesDisabled);

        $('#part').css("display","none");
        $('#all').css("display","none");
        $('#pro').css("display","block");

    });
    $('#partBtn').click(function () {

        $('#partBtn').css(stylesEnable);
        $('#proBtn').css(stylesDisabled);
        $('#toutesBtn').css(stylesDisabled);

        $('#pro').css("display","none");
        $('#all').css("display","none");
        $('#part').css("display","block");

    });


    $(".nav a").on("click", function(){
        $(".nav").find(".active").removeClass("active");
        $(this).parent().addClass("active");
    });


});