<?php

namespace homeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        $view= "indexView";

        $allAnnonces = $this->getDoctrine()->getManager()->getRepository('homeBundle:Annonces')->findAll();
        $parAnnonces = $this->getDoctrine()->getManager()->getRepository('homeBundle:Annonces')->findBy(array('userStatus' => 'part'));
        $proAnnonces = $this->getDoctrine()->getManager()->getRepository('homeBundle:Annonces')->findBy(array('userStatus' => 'pro'));


        return $this->render('homeBundle:Default:index.html.twig', array('allAnnonces' => $allAnnonces,'proAnnonces' => $proAnnonces, 'partAnnonces'=>$parAnnonces , 'view'=>$view));

}


}
