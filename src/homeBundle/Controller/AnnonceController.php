<?php
/**
 * Created by PhpStorm.
 * User: nadjemmejdan
 * Date: 22/04/2017
 * Time: 19:57
 */

namespace homeBundle\Controller;

use homeBundle\Entity\Annonces;
use homeBundle\Form\AnnonceType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class AnnonceController extends Controller
{
    /**
     * @Route("/ajout-annonce")
     */
    public function indexAction(Request $request)
    {
        $user = $this->getUser()->getId();
        $annonce = new Annonces;
        $annonce->setUserId($user);
        $form = $this->createForm(AnnonceType::class, $annonce);
        $view= "addAnnonceView";

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $task = $form->getData();

             $em = $this->getDoctrine()->getManager();
             $em->persist($task);
             $em->flush();

            return $this->redirectToRoute('home_default_index');
        }


        return $this->render('homeBundle:Default:ajoutAnnonce.html.twig', array('view'=>$view, 'form' => $form->createView(),));

    }

    /**
     * @Route("/user/mes-annonces")
     */
    public function userAnnonceAction(Request $request){

        $user = strval($this->getUser()->getId());


        $userAnnonces = $this->getDoctrine()->getManager()->getRepository('homeBundle:Annonces')->findBy(array('user_id' => $user));

        return $this->render('homeBundle:Default:mesAnnonces.html.twig',array('userAnnonces'=>$userAnnonces));
    }

    /**
     * @Route("/user/delete/annonce/{id}", requirements={"page": "\d+"})
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $post = $em->getRepository('homeBundle:Annonces')->find($id);

        if (!$post) {
            return $this->redirectToRoute('home_annonce_userannonce');
        }

        $em->remove($post);
        $em->flush();

        return $this->redirectToRoute('home_annonce_userannonce');
    }

}