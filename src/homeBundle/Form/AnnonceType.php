<?php
/**
 * Created by PhpStorm.
 * User: nadjemmejdan
 * Date: 22/04/2017
 * Time: 16:32
 */

namespace homeBundle\Form;

use homeBundle\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class AnnonceType extends AbstractType
{


    public function __construct()
    {

    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder
            ->add('title',TextType::class)
            ->add('annonce',TextareaType::class)
            ->add('img',TextType::class)
            ->add('prix',TextType::class)
            ->add('user_id',HiddenType::class)
            ->add('userStatus', ChoiceType::class, array('choices'  => array('Particulier' => "part",'professionel' => 'pro'),))
            ->add('ajouter',SubmitType::class, array("attr" => array("class" => "pull-right btn-sm")));

    }

}